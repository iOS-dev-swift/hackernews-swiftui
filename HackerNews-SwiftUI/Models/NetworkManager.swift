//
//  NetworkManager.swift
//  HackerNews-SwiftUI
//
//  Created by Cohen, Dor on 18/10/2020.
//

import Foundation

class NetworkManager: ObservableObject{
    let url: String = "http://hn.algolia.com/api/v1/search?tags=front_page"
    
    @Published var posts: [Post] = []
    
    func fetchPosts(){
        if let url = URL(string: url){
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (data, response, error) in
                if let error = error{
                    print("An error occurred \(error)")
                }else{
                    if let data = data{
                        let decoder = JSONDecoder()
                        do{
                            let posts = try decoder.decode(Posts.self, from: data)
                            DispatchQueue.main.async {
                                self.posts = posts.hits
                            }
                        }catch{
                            print("An error occurred \(error)")
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
}
