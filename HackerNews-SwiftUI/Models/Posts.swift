//
//  Posts.swift
//  HackerNews-SwiftUI
//
//  Created by Cohen, Dor on 18/10/2020.
//

import Foundation

struct Posts: Codable{
    let hits: [Post]
}

struct Post: Codable, Identifiable{
    var id: String{
        return objectID
    }
    let created_at: String
    let title: String
    let author: String
    let _tags: [String]
    let url: String?
    let points: Int
    let objectID: String
}
