//
//  WebView.swift
//  HackerNews-SwiftUI
//
//  Created by Cohen, Dor on 19/10/2020.
//

import SwiftUI
import WebKit

struct WebView: UIViewRepresentable {
    let url: String?
    
    func makeUIView(context: Context) -> WKWebView {
        return WKWebView()
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        if let url = url{
            if let safeUrl = URL(string: url){
                let request = URLRequest(url: safeUrl)
                uiView.load(request)
            }
        }
    }
}


struct WebView_Previews: PreviewProvider {
    static var previews: some View {
        WebView(url: "http://www.google.com")
    }
}
