//
//  PostDetailView.swift
//  HackerNews-SwiftUI
//
//  Created by Cohen, Dor on 19/10/2020.
//

import SwiftUI

struct PostDetailView: View {
    let url: String?
    var body: some View {
        WebView(url: url)
    }
}

struct PostDetailView_Previews: PreviewProvider {
    static var previews: some View {
        PostDetailView(url: "http://www.google.com")
    }
}
