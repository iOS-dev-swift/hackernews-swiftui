//
//  PostView.swift
//  HackerNews-SwiftUI
//
//  Created by Cohen, Dor on 19/10/2020.
//

import SwiftUI

struct PostView: View {
    let post: Post
    var body: some View {
        HStack {
            Text(String(post.points))
                .font(.headline)
                .padding(.trailing)
            Text(post.title)
                .font(.headline)
        }
        .frame(minWidth: 0, maxWidth: .infinity, alignment: .leading)
        .padding()
    }
}

struct PostView_Previews: PreviewProvider {
    static var previews: some View {
        PostView(post: Post(created_at: "2020-10-18T13:52:21.000Z", title: "A Stoic Philosopher in a Hanoi Prison: I am the captain of my soul", author: "stanrivers", _tags: ["story","author_snird","front_page"], url: "https://butwhatfor.substack.com/p/takeaway-friday-a-stoic-philosopher", points: 42, objectID: "24817875")).previewLayout(.sizeThatFits)
    }
}
