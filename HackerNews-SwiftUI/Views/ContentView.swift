//
//  ContentView.swift
//  HackerNews-SwiftUI
//
//  Created by Cohen, Dor on 18/10/2020.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var networkManager = NetworkManager()
    
    init(){
    }
    
    var body: some View {
        NavigationView{
            List {
                ForEach(networkManager.posts){ post in
                    NavigationLink(
                        destination: PostDetailView(url: post.url),
                        label: {
                            PostView(post: post)
                        })
                }
            }
            .listStyle(PlainListStyle())
            .navigationBarTitle("Hacker News")
        }
        .colorScheme(.dark)
        .onAppear {
            self.networkManager.fetchPosts()
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
