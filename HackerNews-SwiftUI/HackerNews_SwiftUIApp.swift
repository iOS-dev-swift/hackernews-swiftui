//
//  HackerNews_SwiftUIApp.swift
//  HackerNews-SwiftUI
//
//  Created by Cohen, Dor on 18/10/2020.
//

import SwiftUI

@main
struct HackerNews_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
